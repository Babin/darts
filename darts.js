var express = require('express');
var app = express();
var path = require('path');
var ip = require("ip");


// Routes
var index   = require('./routes/index');
var player  = require('./routes/player');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Add routes to app
app.use('/', index);
app.use('/', player);

app.use(express.static(path.resolve('./public')));
app.use(express.static(path.resolve('./bower_components')));

var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

var port = app.get('port') || 3001;
var server = server.listen(port, function () {
    
    console.log('Express server(' + ip.address() + ') listening on port ' + port);
});


io.sockets.on('connection', function (socket) {
    socket.on('new_connection', function (client) {
        console.log(client + " connected.");
    });
    socket.on('input', function (input) {
        socket.broadcast.emit('score',input);
    });
    socket.on('command', function (data) {
        socket.broadcast.emit('command',data);
    });
})