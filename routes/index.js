'use strict';
var express = require('express');
var router = express.Router();
var os = require('os');
var interfaces = os.networkInterfaces();
var addresses = [];
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            addresses.push(address.address);
        }
    }
}
/* GET home page. */

var ip;
var port;
if (typeof(process.env.PORT) != "undefined"){
   ip   = 'https://darts-keltpkr.c9users.io';
   port = process.env.PORT;
} else {
    ip      = addresses[0];
    port    = 3001;
}

router.get('/', function (req, res) {
    res.render('index', { port: port ,ip: ip});
});

module.exports = router;