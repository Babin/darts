Darts Score System

-------------------------- Git configuration --------------------------

git config --global user.name "Gérald Babin"
git config --global user.email "kelt_pkr@hotmail.com"
git init
git remote add origin git@gitlab.com:Babin/darts.git

Each next commit :

	git add .
	git commit -m "First commit"
	git push -u origin master
	
Get last remote version :
	git pull
	
Clone :

git clone git@gitlab.com:Babin/darts.git